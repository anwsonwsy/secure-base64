<?php


namespace NWS\SecureBase64;


class CipherAlgorithm
{
    /**
     * Key for encryption and decryption
     *
     * @var int
     */
    private $key;

    /**
     * Force replace characters
     *
     * @var array
     */
    private $forceReplaces = [
        '/' => '$'
    ];

    /**
     * Original characters list
     *
     * @var array
     */
    private $originalCharacters = [
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
        '0','1','2','3','4','5','6','7','8','9','+','/',
    ];

    /**
     * Shifted characters list
     *
     * @var array
     */
    private $shiftedCharacters = [];

    /**
     * CipherAlgorithm constructor.
     *
     * @param int $key
     */
    public function __construct(int $key)
    {
        $this->setKey($key);
        $this->makeShiftedCharacters();
    }

    /**
     * Getter for key
     *
     * @return int
     */
    public function getKey(): int
    {
        return $this->key;
    }

    /**
     * Setter for key
     *
     * @param int $key
     */
    public function setKey(int $key): void
    {
        $this->key = $key >= 64 || $key <= 0 ? 63 : $key;
        $this->makeShiftedCharacters();
    }

    /**
     * Make shifted characters list
     */
    private function makeShiftedCharacters(): void
    {
        $key = $this->getKey();
        $secondPart = array_slice($this->originalCharacters, $key);
        $firstPart = array_slice($this->originalCharacters, 0, $key);
        $this->shiftedCharacters = array_merge($secondPart, $firstPart);
    }

    /**
     * Encrypt base64_encode-ed string
     *
     * @param string $base64String
     * @return string
     */
    public function encrypt(string $base64String): string
    {
        $base64Array = str_split($base64String);

        foreach ($base64Array as &$char) {
            $originalIndex = array_search($char, $this->originalCharacters);
            $char = $this->shiftedCharacters[$originalIndex];
        }

        $encryptedString = join("", $base64Array);

        return $this->runForceReplacements($encryptedString);
    }

    /**
     * Decode encrypted string with cipher
     * and return base64_encode-ed string
     *
     * @param string $encryptedString
     * @return string
     */
    public function decrypt(string $encryptedString): string
    {
        $encryptedString = $this->runForceReplacements($encryptedString, true);
        $encryptedArray = str_split($encryptedString);

        foreach ($encryptedArray as &$char) {
            $shiftedIndex = array_search($char, $this->shiftedCharacters);
            $char = $this->originalCharacters[$shiftedIndex];
        }

        return join("", $encryptedArray);
    }

    /**
     * Run force replacements
     *
     * @param string $string
     * @param bool $reverse
     * @return string
     */
    private function runForceReplacements(string $string, bool $reverse = false): string
    {
        foreach ($this->forceReplaces as $find => $replace) {
            if ($reverse) {
                $string = str_replace($replace, $find, $string);
            } else {
                $string = str_replace($find, $replace, $string);
            }
        }

        return $string;
    }
}