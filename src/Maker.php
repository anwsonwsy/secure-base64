<?php


namespace NWS\SecureBase64;


class Maker
{
    /**
     * Cipher key (default 31)
     *
     * @var int
     */
    private $cipherKey;

    /**
     * Cipher algorithm to run on base64 string
     *
     * @var CipherAlgorithm
     */
    private $algorithm;

    /**
     * Maker constructor.
     *
     * @param int $cipherKey
     */
    public function __construct(?int $cipherKey)
    {
        $this->cipherKey = $cipherKey ?? 31;
        $this->algorithm = new CipherAlgorithm($this->cipherKey);
    }

    /**
     * Change cipher key
     *
     * @param int $cipherKey
     */
    public function setCipherKey(int $cipherKey): void
    {
        $this->cipherKey = $cipherKey;
        $this->algorithm->setKey($cipherKey);
    }

    /**
     * Encrypt data
     *
     * @param string $data
     * @param int|null $cipherKey
     * @return string
     */
    public function encrypt(string $data): string
    {
        return $this->algorithm->encrypt(base64_encode($data));
    }

    /**
     * Decrypt data
     *
     * @param string $encryptedData
     * @param int|null $cipherKey
     * @return string
     */
    public function decrypt(string $encryptedData): string
    {
        return base64_decode($this->algorithm->decrypt($encryptedData));
    }
}