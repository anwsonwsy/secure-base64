# About

This package will make base64 encoding and decoding with cipher key, which knows only
owner.

Composer:

```
composer require nws/secure-base64
```

# Usage 

```php
<?php

use NWS\SecureBase64\Maker as SecureBase64;

$originalText = "Владислав идет куда-то, и говорит по телефону";
$cipherKey = 49; // Integer between 1 and 63
$base64 = new SecureBase64($cipherKey);
$encryptedText = $base64->encrypt($originalText);
$decryptedText = $base64->decrypt($encryptedText);

echo "<pre>";
print_r([
    'original' => $originalText,
    'encrypted' => $encryptedText,
    'decrypted' => $decryptedText
]);
die;
```

Output:

```
<pre>Array                                                                                                                         
(                                                                                                                                  
    [original] => Владислав идет куда-то, и говорит по телефону                                                                    
    [encrypted] => l68Bfuzhl8EBf+3yl8gBd+zj5+zpl8EBeO3z5+zrlJABe+zh8O3zl8pd5+zp5+zkl8sBdezvlJ0Bf+3z5+zwl8pRlJ8BeOzsl8ICS+zvl8oCRhxx
    [decrypted] => Владислав идет куда-то, и говорит по телефону                                                                   
)                                                                                                                                  
```